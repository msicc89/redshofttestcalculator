﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RedSoftTest
{
    class Program
    {

        static char[] symb = { '*', '+', '-', '/', '(', ')' };
        static string action = "1";
        static string errorMessage = "Ваша операция содержит некоторые ошибки. Пожалуйста, попробуйте еще раз!";

        static void Main(string[] args)
        {
            while (action != "0")
            {
                Console.Write("Введите вашу операцию: ");
                string line = Console.ReadLine();
                Console.WriteLine(calc(line));
                keep();
            }
           
        }


    
      

        private static string calc(string result)
        {
            if (hasErrors(result))
                return errorMessage;
            try
            {
                DataTable dt = new DataTable();
                double answer = Convert.ToDouble(dt.Compute(result, ""));
                return result + " = " + answer;
            }
            catch (Exception ex)
            {
                return errorMessage+" \n"+ex.Message;
            }
        }
        

        private static bool IsValidSymb(char input)
        {
            return symb.Contains(input);
        }

        private static bool hasErrors(string input)
        {
            int errors = 0;
            for (int i = 0; i < input.Length; i++)
            {
            
                if (!IsNumber(input[i]))
                    if (!IsValidSymb(input[i]))
                        errors++;
                if (i == 0)
                    if (IsValidSymb(input[i]))
                        errors++;
                if ((i+1) == input.Length)
                    if (IsValidSymb(input[i]))
                        errors++;
            }
            return errors > 0;
        }

        private static bool IsNumber(char t)
        {
            double parse;
            return double.TryParse(t.ToString(), out parse);
        }

        private static void keep()
        {
            Console.Write("Для выхода нажмите 0.\n Для продолжения нажмите любую клавишу....");
            action = Console.ReadKey().KeyChar.ToString();
            Console.WriteLine("\n");
        }
    }
}